# Simple Flask Demo

This repository contains a simple flask demo. There are two separate flask apps:

1. flask_get_example.py : a very simple example with a single HTTP GET endpoint using a single Path Variable.
2. flask_post_example.py : a simple example which also contains a HTTP POST endpoint.

## Installing on Amazon Linux 2

The steps required to get this runnning on Amazon Linux 2 are:


```
sudo yum install -y python3-pip python3 python3-setuptools
sudo yum install -y git
git clone https://bitbucket.org/fcallaly/simple-flask-demo.git
sudo pip3 install -r requirements.txt
python3 flask_get_example.py
```