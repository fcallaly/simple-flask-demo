import logging

from flask import Flask, request
from flask_restx import Resource, Api, fields

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)

ns = api.namespace('v1/review', description='Review operations')

review = api.model('Review', {
    'review': fields.String(required=True, description='The review text')
    })


@ns.route('/')
class Review(Resource):
    def get(self):
        return {'test': 'reviews'}

    @ns.expect(review)
    def post(self):
        LOG.info('Data posted: ' + api.payload['review'])
        return {'request': api.payload['review']}


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    app.run(debug=True)
