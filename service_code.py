import logging

LOG = logging.getLogger(__name__)


# this is an example function to perform some logic
def example_service(example_parameter):
    LOG.info("Example service function called with parameter: " +
             str(example_parameter))
