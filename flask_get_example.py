from datetime import datetime, timedelta
import logging

from flask import Flask
from flask_restx import Resource, Api

import service_code

app = Flask(__name__)
api = Api(app)


# Expecting a ticker as a path variable
@api.route('/v1/example/<string:example_parameter>')
@api.param('example_parameter', 'An example path variable')
class Example(Resource):
    def get(self, example_parameter):

        # call the "service" code here e.g.
        service_code.example_service(example_parameter)

        return {'example_parameter': example_parameter,
                'response': 'some response'}


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    app.run(host="0.0.0.0", debug=True)
